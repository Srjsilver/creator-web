import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {StepperOverviewComponent} from './stepper-overview/stepper-overview.component';
import {OtherProjectsComponent} from './other-projects/other-projects.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'project', component: StepperOverviewComponent},
  {path: 'home', component: HomeComponent},
  {path: 'otherProjects', component: OtherProjectsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
