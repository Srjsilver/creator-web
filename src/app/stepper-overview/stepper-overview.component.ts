import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {saveAs} from 'file-saver';
import {group} from '@angular/animations';

@Component({
  selector: 'app-stepper-overview',
  templateUrl: './stepper-overview.component.html',
  styleUrls: ['./stepper-overview.component.css']
})
export class StepperOverviewComponent implements OnInit {

  isLinear = true;
  javaVersions: string[]  = ['Java7', 'Java8', 'Java9'];
  primaryKeyType: string[] = ['Long', 'String'];
  databaseDriverclassNameOptions: string[] = ['org.mariadb.jdbc.Driver'];

  projectFormGroup: FormGroup;
  dataFormGroup: FormGroup;
  serviceFormGroup: FormGroup;
  databaseFormGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder, private http: HttpClient) {
  }

  ngOnInit() {
    this.projectFormGroup = this._formBuilder.group({
      projectName: [
        '', Validators.required],
      projectDescription: [
        '', Validators.required],
      packageData: this._formBuilder.group({
        group: [
          '', Validators.required],
        flyway: [
          '', Validators.required],
        springBoot: [
          '', Validators.required],
        sourceCompatibilityJavaVersion: [
          'Java8', Validators.required],
        dependencyList: this._formBuilder.array([]),
        databaseData: this._formBuilder.group({
          databaseDriverClassName: [
            '', Validators.required],
          databaseUri: [
            '', Validators.required],
          databaseUsername: [
            ''],
          databasePassword: [
            '']
        })
      }),
      serviceData: this._formBuilder.group({
        jsonString: [
          '', Validators.required],
        primaryKey: [
          '', Validators.required],
        primaryKeyType: [
          '', Validators.required],
        dataPrimaryType: [
          '', Validators.required]
      })
    });
    this.dataFormGroup = <FormGroup>(this.projectFormGroup.get('packageData'));
    this.databaseFormGroup = <FormGroup>(this.dataFormGroup.get('databaseData'));
    this.serviceFormGroup = <FormGroup>(this.projectFormGroup.get('serviceData'));
  }
  doDownload() {
    const serviceGeneratorUrl = 'https://sb-gcp-dot-solrest-servicegenerator-v1.appspot.com/gradle/create';
    const localServiceGeneratorUrl = 'http://127.0.0.1:8080/gradle/create';
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/zip'
    });
    const form: FormGroup = this.projectFormGroup;
    const obj: string = JSON.stringify(form.value);
    this.http.post(localServiceGeneratorUrl, obj, {headers, responseType: 'blob'}).subscribe(
      data => {
        saveAs(data, 'microservice.zip');
      },
      error => {
        console.log(error);
      }
    );
  }
  isProjectValid() {
    return this.projectFormGroup.get('projectName').valid && this.projectFormGroup.get('projectDescription').valid;
  }

}
